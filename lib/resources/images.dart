part of 'resources.dart';

class Images {
  Images._();

  static const String avatar = 'assets/png/avatar.png';
  static const String filter = 'assets/png/filter.png';
  static const String folder = 'assets/png/folder.png';
  static const String menuIcon = 'assets/png/menu_icon.png';
}
