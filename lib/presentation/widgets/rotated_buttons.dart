import 'package:file_manager_project/presentation/themes/app_colors.dart';
import 'package:file_manager_project/presentation/themes/app_fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RotatedButtons extends StatelessWidget {
  const RotatedButtons({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        RotatedBox(
          quarterTurns: 3,
          child: Text(
            "Internal storage",
            style: AppFonts.s16W700.copyWith(color: AppColors.yellow),
          ),
        ),
        SizedBox(height: 56.h),
        RotatedBox(
          quarterTurns: 3,
          child: Text(
            "External storage",
            style: AppFonts.s14W400.copyWith(color: Colors.white),
          ),
        ),
      ],
    );
  }
}
