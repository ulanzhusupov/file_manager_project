import 'package:flutter/material.dart';

class HomeBackgroundWhite extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    // Square

    Paint paintFill1 = Paint()
      ..color = const Color.fromARGB(255, 255, 255, 255)
      ..style = PaintingStyle.fill
      ..strokeWidth = size.width * 0.00
      ..strokeCap = StrokeCap.butt
      ..strokeJoin = StrokeJoin.miter;

    Path path_1 = Path();
    path_1.moveTo(0, size.height * 0.5899281);
    path_1.cubicTo(
        size.width * 0.0026667,
        0,
        size.width * 1.0025333,
        size.height * 0.6355036,
        size.width * 1.0025333,
        size.height * 0.0024101);
    path_1.quadraticBezierTo(size.width * 0.9998667, size.height * 0.2050360,
        size.width * 1.0008533, size.height * 1.0065108);
    path_1.lineTo(size.width * 0.0008800, size.height * 1.0011511);
    path_1.quadraticBezierTo(size.width * -0.0026667, size.height * 0.8705036,
        0, size.height * 0.5899281);
    path_1.close();

    canvas.drawPath(path_1, paintFill1);

    // Square

    Paint paintStroke1 = Paint()
      ..color = const Color.fromARGB(255, 255, 255, 255)
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.00
      ..strokeCap = StrokeCap.butt
      ..strokeJoin = StrokeJoin.miter;

    canvas.drawPath(path_1, paintStroke1);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
