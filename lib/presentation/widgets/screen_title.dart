import 'package:file_manager_project/presentation/themes/app_fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ScreenTitle extends StatelessWidget {
  const ScreenTitle({
    super.key,
    required this.mainTitle,
    // required this.subtitle,
  });

  final String mainTitle;
  // final String subtitle;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              mainTitle,
              style: AppFonts.s32W700.copyWith(color: Colors.white),
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(top: 10.h),
          child: InkWell(
            onTap: () {},
            child: Row(
              children: [
                const CircleAvatar(
                  radius: 3,
                  backgroundColor: Colors.white,
                ),
                SizedBox(width: 5.w),
                const CircleAvatar(
                  radius: 3,
                  backgroundColor: Colors.white,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
