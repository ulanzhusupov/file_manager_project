import 'package:file_manager_project/presentation/themes/app_colors.dart';
import 'package:file_manager_project/presentation/themes/app_fonts.dart';
import 'package:file_manager_project/resources/resources.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MemoryContainer extends StatelessWidget {
  final int totalDiskSpace;
  final int totalUsedSpace;
  final int freeSpace;
  final int usedSpacePercent;

  const MemoryContainer({
    super.key,
    required this.totalUsedSpace,
    required this.freeSpace,
    required this.usedSpacePercent,
    required this.totalDiskSpace,
  });

  @override
  Widget build(BuildContext context) {
    // final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    // deviceInfoPlugin.androidInfo.
    return Container(
      width: 234.w,
      height: 422.h,
      decoration: BoxDecoration(
        color: AppColors.redAccent,
        borderRadius: BorderRadius.circular(26.r),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 30.h, horizontal: 20.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  "Your storage",
                  style: AppFonts.s21W700,
                ),
                InkWell(
                  onTap: () {},
                  child: Image.asset(
                    Images.filter,
                    width: 24,
                  ),
                ),
              ],
            ),
            SizedBox(height: 30.h),
            SizedBox(
              width: 164.w,
              height: 164.h,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  PieChart(
                    PieChartData(
                      sectionsSpace: 20,
                      startDegreeOffset: 90,
                      borderData: FlBorderData(
                        show: true,
                        border: Border.all(
                          width: 1,
                          color: Colors.black,
                        ),
                      ),
                      sections: [
                        PieChartSectionData(
                          title: "${freeSpace}GB",
                          titleStyle: AppFonts.s10W700,
                          value: freeSpace.toDouble(),
                          color: AppColors.yellow,
                          radius: 40,
                          showTitle: true,
                        ),
                        PieChartSectionData(
                          title: "${totalUsedSpace}GB",
                          titleStyle:
                              AppFonts.s10W700.copyWith(color: Colors.black),
                          value: totalUsedSpace.toDouble(),
                          color: Colors.white,
                          radius: 45,
                          showTitle: true,
                        ),
                      ],
                      centerSpaceRadius: 62.r,
                    ),
                    swapAnimationDuration:
                        const Duration(milliseconds: 150), // Optional
                    swapAnimationCurve: Curves.linear, // Optional
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "${totalDiskSpace}GB",
                        style: AppFonts.s21W700,
                      ),
                      Text(
                        "$usedSpacePercent% USED",
                        style: AppFonts.s12W700,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 70.h),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "● Used",
                  style: AppFonts.s16W700,
                ),
                SizedBox(width: 15.w),
                Text(
                  "● Free",
                  style: AppFonts.s16W700.copyWith(color: Colors.yellow),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
