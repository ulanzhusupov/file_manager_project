import 'package:auto_route/auto_route.dart';
import 'package:disk_space_update/disk_space_update.dart';
import 'package:file_manager_project/presentation/themes/app_colors.dart';
import 'package:file_manager_project/presentation/themes/app_fonts.dart';
import 'package:file_manager_project/presentation/widgets/memory_container.dart';
import 'package:file_manager_project/presentation/widgets/rotated_buttons.dart';
import 'package:file_manager_project/presentation/widgets/screen_title.dart';
import 'package:file_manager_project/resources/resources.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

@RoutePage()
class MyStorageInfoPage extends StatefulWidget {
  const MyStorageInfoPage({super.key});

  @override
  State<MyStorageInfoPage> createState() => _MyStorageInfoPageState();
}

class _MyStorageInfoPageState extends State<MyStorageInfoPage> {
  int? _totalDiskSpace;
  int? _freeSpace;
  int? _usedSpace;
  int? _usedSpacePercent;

  @override
  void initState() {
    super.initState();
    getSpaceInfo();
  }

  Future<void> getSpaceInfo() async {
    double diskFreeSpace = await DiskSpace.getFreeDiskSpace ?? 0;
    double totalDiskSpace = await DiskSpace.getTotalDiskSpace ?? 0;

    setState(() {
      _freeSpace = (diskFreeSpace / 1024).ceil();
      _totalDiskSpace = (totalDiskSpace / 1024).ceil();
      _usedSpace = _totalDiskSpace! - _freeSpace!;
      _usedSpacePercent = ((_usedSpace! / _totalDiskSpace!) * 100).ceil();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.appBg,
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(0, 1, 25, 1),
        leading: Padding(
          padding: EdgeInsets.only(left: 30.w),
          child: InkWell(
            child: Image.asset(
              Images.menuIcon,
              width: 24.w,
            ),
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 30.w),
            child: InkWell(
              child: Image.asset(
                Images.avatar,
                width: 34,
              ),
            ),
          ),
        ],
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            width: double.infinity,
            height: 120.h,
            color: Colors.white,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 30.w),
            child: Column(
              children: [
                SizedBox(height: 50.h),
                const ScreenTitle(mainTitle: "File\nManager"),
                Row(
                  children: [
                    Text(
                      "Let’s clean and ",
                      style: AppFonts.s13W500.copyWith(color: Colors.white),
                    ),
                    Text(
                      "manage your file’s.",
                      style: AppFonts.s13W500.copyWith(color: AppColors.yellow),
                    ),
                  ],
                ),
                SizedBox(height: 54.h),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const RotatedButtons(),
                    SizedBox(width: 36.w),
                    MemoryContainer(
                      totalDiskSpace: _totalDiskSpace ?? 0,
                      totalUsedSpace: _usedSpace ?? 0,
                      freeSpace: _freeSpace ?? 0,
                      usedSpacePercent: _usedSpacePercent ?? 0,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
