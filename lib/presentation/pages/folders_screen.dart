import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:file_manager/file_manager.dart';
import 'package:file_manager_project/presentation/themes/app_colors.dart';
import 'package:file_manager_project/presentation/themes/app_fonts.dart';
import 'package:file_manager_project/presentation/widgets/screen_title.dart';
import 'package:file_manager_project/resources/resources.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:permission_handler/permission_handler.dart';

@RoutePage()
class FoldersPage extends StatefulWidget {
  const FoldersPage({super.key});

  @override
  State<FoldersPage> createState() => _FoldersPageState();
}

class _FoldersPageState extends State<FoldersPage> {
  final FileManagerController controller = FileManagerController();
  @override
  void initState() {
    super.initState();
    requestPermission();
  }

  Future<void> requestPermission() async {
    await FileManager.requestFilesAccessPermission();

    // Проверяем статус разрешения
    // if (status == PermissionStatus.granted) {
    //   // Разрешение предоставлено, выполните нужные действия
    //   print('Разрешение получено');
    // } else {
    //   // Разрешение не предоставлено, предоставьте пользователю информацию или запросите снова
    //   print('Разрешение не получено');
    // }
    // setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ControlBackButton(
      controller: controller,
      child: Scaffold(
        backgroundColor: const Color.fromRGBO(0, 1, 25, 1),
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(0, 1, 25, 1),
          leading: Padding(
            padding: EdgeInsets.only(left: 30.w),
            child: InkWell(
              child: Image.asset(
                Images.menuIcon,
                width: 24.w,
              ),
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 30.w),
              child: InkWell(
                child: Image.asset(
                  Images.avatar,
                  width: 34,
                ),
              ),
            ),
          ],
        ),
        body: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              height: 565.h,
              padding: EdgeInsets.symmetric(horizontal: 30.w),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
                color: Colors.white,
              ),
              child: FileManager(
                controller: controller,
                builder: (context, snapshot) {
                  final List<FileSystemEntity> entities = snapshot;
                  return ListView.builder(
                    itemCount: entities.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: InkWell(
                          onTap: () {
                            if (FileManager.isDirectory(entities[index])) {
                              controller.openDirectory(
                                  entities[index]); // open directory
                            } else {
                              // Perform file-related tasks.
                              print("This is file");
                            }
                          },
                          child: Container(
                            padding: const EdgeInsets.all(20),
                            decoration: const BoxDecoration(
                                color: AppColors.folderBg,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                FileManager.isFile(entities[index])
                                    ? const Icon(Icons.feed_outlined)
                                    : Image.asset(
                                        Images.folder,
                                        width: 50.w,
                                      ),
                                SizedBox(width: 27.w),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      child: Text(
                                        FileManager.basename(entities[index]),
                                        style: AppFonts.s15W600
                                            .copyWith(color: Colors.black),
                                      ),
                                    ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          0.5,
                                      child: subtitle(entities[index]),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 50.h),
                  const ScreenTitle(
                    mainTitle: "Orix Designers",
                  ),
                  SizedBox(height: 17.h),
                  const Text(
                    "Home · My storage · Figma file",
                    style: AppFonts.s14W700,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget subtitle(FileSystemEntity entity) {
    return FutureBuilder<FileStat>(
      future: entity.stat(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (entity is File) {
            int size = snapshot.data!.size;

            return Text(FileManager.formatBytes(size),
                style: AppFonts.s15W600.copyWith(
                  color: Colors.black,
                ));
          }
          return Text(
            "${snapshot.data!.modified}".substring(0, 10),
            style: AppFonts.s15W600.copyWith(
              color: Colors.black,
            ),
          );
        } else {
          return const Text("");
        }
      },
    );
  }
}


/*
Card(
  child: ListTile(
    leading: FileManager.isFile(entities[index])
        ? const Icon(Icons.feed_outlined)
        : const Icon(Icons.folder),
    title: Text(FileManager.basename(entities[index])),
    onTap: () {
      if (FileManager.isDirectory(entities[index])) {
        controller.openDirectory(
            entities[index]); // open directory
      } else {
        // Perform file-related tasks.
      }
    },
  ),
)
*/


