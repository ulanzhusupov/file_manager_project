import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color appBg = Color(0xff000119);
  static const Color yellow = Color(0xffFFD032);
  static const Color redAccent = Color(0xffE3001A);
  static const Color folderBg = Color(0xfff8f8f8);
}
