import 'package:flutter/material.dart';

abstract class AppFonts {
  static const TextStyle s32W700 = TextStyle(
    fontSize: 32,
    fontWeight: FontWeight.w700,
    color: Colors.white,
  );

  static const TextStyle s32W400 = TextStyle(
    fontSize: 32,
    fontWeight: FontWeight.w400,
    color: Colors.white,
  );

  static const TextStyle s13W500 = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w500,
    color: Colors.white,
  );

  static const TextStyle s13W700 = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w700,
    color: Colors.white,
  );

  static const TextStyle s15W700 = TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w700,
    color: Colors.white,
  );

  static const TextStyle s16W700 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w700,
    color: Colors.white,
  );

  static const TextStyle s17W700 = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w700,
    color: Colors.white,
  );

  static const TextStyle s14W700 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w700,
    color: Colors.white,
  );

  static const TextStyle s14W400 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    color: Colors.white,
  );

  static const TextStyle s12W700 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w700,
    color: Colors.white,
  );

  static const TextStyle s12W600 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w600,
    color: Colors.white,
  );

  static const TextStyle s10W700 = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w700,
    color: Colors.black,
  );

  static const TextStyle s15W600 = TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w600,
    color: Colors.white,
  );

  static const TextStyle s24W700 = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.w700,
    color: Colors.white,
  );

  static const TextStyle s21W700 = TextStyle(
    fontSize: 21,
    fontWeight: FontWeight.w700,
    color: Colors.white,
  );
}
