import 'package:auto_route/auto_route.dart';
import 'package:file_manager_project/presentation/pages/files_page.dart';
import 'package:file_manager_project/presentation/pages/folders_screen.dart';
import 'package:file_manager_project/presentation/pages/home_page.dart';
import 'package:file_manager_project/presentation/pages/my_storage_info.dart';
import 'package:flutter/material.dart';

part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          page: HomeRoute.page,
        ),
        AutoRoute(
          page: MyStorageInfoRoute.page,
        ),
        AutoRoute(
          page: FoldersRoute.page,
          initial: true,
        ),
        AutoRoute(
          page: FilesRoute.page,
        ),
      ];
}
