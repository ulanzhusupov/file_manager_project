import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:file_manager_project/resources/resources.dart';

void main() {
  test('images assets test', () {
    expect(File(Images.avatar).existsSync(), isTrue);
    expect(File(Images.filter).existsSync(), isTrue);
    expect(File(Images.folder).existsSync(), isTrue);
    expect(File(Images.menuIcon).existsSync(), isTrue);
  });
}
